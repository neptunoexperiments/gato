function love.load()
  image = love.graphics.newImage("cu.png")
  sound = love.audio.newSource("Tarzan.mp3", "static")
  
  -- tela
  larguraTela = 1080
  alturaTela = 1928
  love.window.setMode(larguraTela, alturaTela)
  
  -- player
  largura = 30
  altura = 30
  
  -- medidas
  posx = 50
  posy = 50
  direction = ""
  
  --body = love.physics.newBody(World, x, y, type)
 
end

function love.update(dt)
  
 love.audio.play(sound)
 
end

function love.draw()
  
  love.graphics.draw(image, 5, 100)

    local touches = love.touch.getTouches()

    for i, id in ipairs(touches) do
        local x, y = love.touch.getPosition(id)
        love.graphics.circle("fill", x, y, 20)
    end
end